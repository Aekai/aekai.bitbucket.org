
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var tabTez = [];
var tabVis = [];
var tabITM = [];

var tabPac = [ "", "7a93dec0-53b5-43b2-86e0-0aa68a0b1646", "15b42618-b2ca-469c-af9c-219d5e70915d", "3c8dcde8-2952-4aed-bd08-0f18544211eb"];
/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
 
 function generirajPodatke(stPacienta) {
 	
 	generiranEhrID = 0;
 	
 	if (stPacienta == 1) {
 		$("#Ime").val("Sabina");
		$("#Priimek").val("Suha");
		$("#DatumRojstva").val("1999-10-30");
		/////////////////////////////////////////////////
		
		ehrId = "";
  
		  sessionId = getSessionId();
		
			var ime = $("#Ime").val();
			var priimek = $("#Priimek").val();
		  var datumRojstva = $("#DatumRojstva").val() + "T00:00:00.000Z";
		
				$.ajaxSetup({
				    headers: {"Ehr-Session": sessionId}
				});
				$.ajax({
				    url: baseUrl + "/ehr",
				    type: 'POST',
				    success: function (data) {
				        var ehrId = data.ehrId;
				        var partyData = {
				            firstNames: ime,
				            lastNames: priimek,
				            dateOfBirth: datumRojstva,
				            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
				        };
				        $.ajax({
				            url: baseUrl + "/demographics/party",
				            type: 'POST',
				            contentType: 'application/json',
				            data: JSON.stringify(partyData),
				            success: function (party) {
				                if (party.action == 'CREATE') {
				                    $("#kreirajSporocilo").html("<span class='obvestilo " +
		                          "label label-success fade-in'>Uspešno kreiran EHR '" +
		                          ehrId + "'.</span>");
		                        	tabPac[1] = ehrId;
                        			generiranEhrID = ehrId;
				                    $("#EhrID").val(ehrId);
				                }
				            },
				            error: function(err) {
				            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
		                    "label-danger fade-in'>Napaka '" +
		                    JSON.parse(err.responseText).userMessage + "'!");
				            }
				        });
				    }
				});

		///////////////////////////////////////////////////
		setTimeout(function() {for (var i = 0; i < 4; i++) {
			
			$("#Visina").val((160+i));
			$("#Teza").val((44+i));
			$("#DatumMeritve").val(2014+i + "-11-"+(21+i)+"T11:40Z");
			setTimeout(posljiPodatke(), 10);
		}}, 100);
		setTimeout(function() {generirajPodatke(2)}, 1000);
 	}
 	else if (stPacienta == 2) {
 		$("#Ime").val("Franci");
		$("#Priimek").val("Fit");
		$("#DatumRojstva").val("1989-12-12");
		/////////////////////////////////////////////////
		
		ehrId = "";
  
		  sessionId = getSessionId();
		
			var ime = $("#Ime").val();
			var priimek = $("#Priimek").val();
		  var datumRojstva = $("#DatumRojstva").val() + "T00:00:00.000Z";
		
				$.ajaxSetup({
				    headers: {"Ehr-Session": sessionId}
				});
				$.ajax({
				    url: baseUrl + "/ehr",
				    type: 'POST',
				    success: function (data) {
				        var ehrId = data.ehrId;
				        var partyData = {
				            firstNames: ime,
				            lastNames: priimek,
				            dateOfBirth: datumRojstva,
				            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
				        };
				        $.ajax({
				            url: baseUrl + "/demographics/party",
				            type: 'POST',
				            contentType: 'application/json',
				            data: JSON.stringify(partyData),
				            success: function (party) {
				                if (party.action == 'CREATE') {
				                    $("#kreirajSporocilo").html("<span class='obvestilo " +
		                          "label label-success fade-in'>Uspešno kreiran EHR '" +
		                          ehrId + "'.</span>");
		                          tabPac[2] = ehrId;
                          			generiranEhrID = ehrId;
				                    $("#EhrID").val(ehrId);
				                }
				            },
				            error: function(err) {
				            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
		                    "label-danger fade-in'>Napaka '" +
		                    JSON.parse(err.responseText).userMessage + "'!");
				            }
				        });
				    }
				});

		///////////////////////////////////////////////////
		setTimeout(function() {for (var i = 0; i < 4; i++) {
			
			$("#Visina").val(176 + (i==1 ? 0 : 1));
			$("#Teza").val(61 + Math.round((i+1)/2));
			$("#DatumMeritve").val(2014+i + "-05-"+(11+i)+"T13:22Z");
			setTimeout(function() {posljiPodatke()}, 10);
		}}, 500);
		setTimeout(function() {generirajPodatke(3)}, 1000);
 	}
 	else {
 		$("#Ime").val("Domen");
		$("#Priimek").val("Debel");
		$("#DatumRojstva").val("1969-04-20");
		/////////////////////////////////////////////////
		
		ehrId = "";
  
		  sessionId = getSessionId();
		
			var ime = $("#Ime").val();
			var priimek = $("#Priimek").val();
		  var datumRojstva = $("#DatumRojstva").val() + "T00:00:00.000Z";
		
				$.ajaxSetup({
				    headers: {"Ehr-Session": sessionId}
				});
				$.ajax({
				    url: baseUrl + "/ehr",
				    type: 'POST',
				    success: function (data) {
				        var ehrId = data.ehrId;
				        var partyData = {
				            firstNames: ime,
				            lastNames: priimek,
				            dateOfBirth: datumRojstva,
				            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
				        };
				        $.ajax({
				            url: baseUrl + "/demographics/party",
				            type: 'POST',
				            contentType: 'application/json',
				            data: JSON.stringify(partyData),
				            success: function (party) {
				                if (party.action == 'CREATE') {
				                    $("#kreirajSporocilo").html("<span class='obvestilo " +
		                          "label label-success fade-in'>Uspešno kreiran EHR '" +
		                          ehrId + "'.</span>");
	                          		tabPac[3] = ehrId;
	                          		generiranEhrID = ehrId;
				                    $("#EhrID").val(ehrId);
				                }
				            },
				            error: function(err) {
				            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
		                    "label-danger fade-in'>Napaka '" +
		                    JSON.parse(err.responseText).userMessage + "'!");
				            }
				        });
				    }
				});

		///////////////////////////////////////////////////

		setTimeout(function() {for (var i = 0; i < 4; i++) {
			
			$("#Visina").val(181);
			$("#Teza").val(101+Math.round((i+1)/2));
			$("#DatumMeritve").val(2014+i + "-04-"+20+"T02:10Z");
			setTimeout(posljiPodatke(), 10);
		}}, 500);
 	}
 	
 	return generiranEhrID;
 }
 
 

/*
funkcija za kreiranje novih EHR zapisov
*/
 
function kreirajEhr() {
  ehrId = "";
  
  sessionId = getSessionId();

	var ime = $("#Ime").val();
	var priimek = $("#Priimek").val();
  var datumRojstva = $("#DatumRojstva").val() + "T00:00:00.000Z";

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
		                    $("#EhrID").val(ehrId);
		                }
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
	}
	
}

/*
funkcija za pošiljanje novih meritev
*/
function posljiPodatke() {
	sessionId = getSessionId();

	var ehrId = $("#EhrID").val();
	var datumInUra = $("#DatumMeritve").val();
	var telesnaVisina = $("#Visina").val();
	var telesnaTeza = $("#Teza").val();
	var telesnaTemperatura = 0;
	var sistolicniKrvniTlak = 0;
	var diastolicniKrvniTlak = 0;
	var nasicenostKrviSKisikom = 0;
	var merilec = "Uporabnik";

	if (!ehrId || ehrId.trim().length == 0) {
		$("#posljiMeritveSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
		    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: merilec
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        $("#posljiMeritveSporocilo").html(
              "<span class='obvestilo label label-success fade-in'>" +
              res.meta.href + ".</span>");
		    },
		    error: function(err) {
		    	$("#posljiMeritveSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
		    }
		});
	}
}

/*
funkcija za pridobivanje podatkov meritev iz EHRscape
*/

function pridobiPodatke() {
	sessionId = getSessionId();

	var ehrId = $("#EhrID").val();
	var tip = $("#izbranAtribut").val();
	if (!ehrId || ehrId.trim().length == 0 || !tip || tip.trim().length == 0) {
		$("#PreberiPodatkeSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
	    		var party = data.party;
	    		$("#Ime").val(party.firstNames);
	    		$("#Priimek").val(party.lastNames);
	    		$("#DatumRojstva").val(party.dateOfBirth);
				if (tip == "1") {
					$.ajax({
  					    url: baseUrl + "/view/" + ehrId + "/" + "height",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	if (res.length > 0) {
						    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>Telesna Višina</th></tr>";
                    			tabVis = [];
						        for (var i in res) {
						        	tabVis.unshift([res[i].time, res[i].height]);
						            results += "<tr><td>" + res[i].time +
                          "</td><td class='text-right'>" + res[i].height +
                          " " + res[i].unit + "</td></tr>";
						        }
						        results += "</table>";
						        $("#rezultatMeritveVitalnihZnakov").html = "";
						        $("#rezultatMeritveVitalnihZnakov").html(results);
						        tabVis.unshift(["Datum", "Višina [cm]"]);
						        drawChart(tip, tabVis);
						        
					    	} else {
					    		$("#PreberiPodatkeSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}
					    },
					    error: function() {
					    	$("#PreberiPodatkeSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
				} else if (tip == "2") {
					$.ajax({
					    url: baseUrl + "/view/" + ehrId + "/" + "weight",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	if (res.length > 0) {
						    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>Telesna teža</th></tr>";
                    			tabTez = [];
						        for (var i in res) {
						        	tabTez.unshift([res[i].time, res[i].weight]);
						            results += "<tr><td>" + res[i].time +
                          "</td><td class='text-right'>" + res[i].weight + " " 	+
                          res[i].unit + "</td></tr>";
						        }
						        results += "</table>";
						        $("#rezultatMeritveVitalnihZnakov").html = "";
						        $("#rezultatMeritveVitalnihZnakov").html(results);
						        // visualizacija grafa teže
								tabTez.unshift(["Datum", "Teža [kg]"]);
								drawChart(tip, tabTez);
								
					        	// /graf teže
					    	} else {
					    		$("#PreberiPodatkeSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}
					    },
					    error: function() {
					    	$("#PreberiPodatkeSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
				}
				else if (tip == "3") {
					var results = "";
					tabITM = [];
					tabITM.unshift(["Datum", "ITM"]);
					for (var i = 1; i < tabTez.length; i++) {
						tabITM.push([tabTez[i][0], tabTez[i][1]*10000/(tabVis[i][1]*tabVis[i][1])]);
						results = "<tr><td>" + tabITM[i][0] +
                          "</td><td class='text-right'>" + tabITM[i][1] + "</td></tr>" + results;
					}
					results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>ITM</th></tr>" + results;
					$("#rezultatMeritveVitalnihZnakov").html = "";
			        $("#rezultatMeritveVitalnihZnakov").html(results);
					drawChart(tip, tabITM);
					prikaziObrazlozitev(tabITM.pop().pop());
				}
	    	},
	    	error: function(err) {
	    		$("#PreberiPodatkeSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
	    	}
		});
	}
}




// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija

					

//test
$(document).ready( () => {
	
	$("#genSel").change(() => {
		
		$("#EhrID").val(tabPac[ $("#genSel").val() ]);
		
	});
	
	
	$("#izbranAtribut").change(() => {
		
		$("#rezultatMeritveVitalnihZnakov").innerHTML = "";
		$("#poljeZaSliko").html('');
		$("#poljeZaObrazlozitev").html('');
		$("#poljeZaTabelo").html('');
		pridobiPodatke();
		
	});
	

	

});


function prikaziObrazlozitev(itm) {
	
	if(itm < 18.5) {
		$("#poljeZaSliko").html('<img src="slike/prenizekItm.jpg" style="width:100px;height:100px;" alt="prenizekItm.jpg"></img>');
		$("#poljeZaObrazlozitev").html('Vaš ITM spada v skupino "podhranjenost", priporočamo, da se posvetujete z zdravnikom.');
	}
	else if (itm > 30) {
		$("#poljeZaSliko").html('<img src="slike/debelItm.jpg" style="width:100px;height:100px;" alt="debelItm.jpg"></img>');
		$("#poljeZaObrazlozitev").html('Vaš ITM spada v skupino "debelost". Previsoka telesna masa lahko privede do pojava sladkorne bolezni in poveča tveganje za marsikatera druga obolenja. Priporočamo, da se posvetujete z zdravnikom.');
	}
	else if (itm > 25) {
		$("#poljeZaSliko").html('<img src="slike/previsokItm.jpg" style="width:100px;height:100px;" alt="previsokItm.jpg"></img>');
		$("#poljeZaObrazlozitev").html('Vaš ITM spada v skupino "čezmerna hranjenost". Previsoka telesna masa lahko privede do pojava sladkorne bolezni in poveča tveganje za marsikatera druga obolenja. Priporočamo, da se posvetujete z zdravnikom.');
	}
	else {
		$("#poljeZaSliko").html('<img src="slike/primerenItm.jpg" style="width:100px;height:100px;" alt="primerenItm.jpg"></img>');
		$("#poljeZaObrazlozitev").html('Vaš ITM spada v skupino "normalna hranjenost", kar tako naprej!');
	}

	$("#poljeZaObrazlozitev").append('<br>ITM se izračuna po formuli: <b>(teža v kg) / (višina v m)^2</b> . Spodaj si lahko pogledate tabelo s statistiko ITM za Slovenijo.');
	
	$("#poljeZaTabelo").append('<tbody> \
			<tr> \
				<th class="layout2-table-stub layout2-table-stub1" id="R00" rowspan="28" scope="row">Spol - SKUPAJ</th><th class="layout2-table-stub layout2-table-stub2" id="R10" rowspan="4" scope="row">15-24 let</th><th class="layout2-table-stub layout2-table-stub3" id="R00" rowspan="1" scope="row">Podhranjenost</th><td class="table-data-filled" headers="R00 R10 R00 H101">5,9</td> \
			</tr><tr>\
				<th class="layout2-table-stub layout2-table-stub3" id="R11" rowspan="1" scope="row">Normalna hranjenost</th><td class="table-data-filled" headers="R00 R10 R11 H101">70,6</td> \
			</tr><tr> \
				<th class="layout2-table-stub layout2-table-stub3" id="R22" rowspan="1" scope="row">Čezmerna hranjenost</th><td class="table-data-filled" headers="R00 R10 R22 H101">18,4</td> \
			</tr><tr>\
				<th class="layout2-table-stub layout2-table-stub3" id="R33" rowspan="1" scope="row">Debelost</th><td class="table-data-filled" headers="R00 R10 R33 H101">5,1</td>\
			</tr><tr>\
				<th class="layout2-table-stub layout2-table-stub2" id="R24" rowspan="4" scope="row">25-34 let</th><th class="layout2-table-stub layout2-table-stub3" id="R04" rowspan="1" scope="row">Podhranjenost</th><td class="table-data-filled" headers="R00 R24 R04 H101">3,4</td>\
			</tr><tr>\
				<th class="layout2-table-stub layout2-table-stub3" id="R15" rowspan="1" scope="row">Normalna hranjenost</th><td class="table-data-filled" headers="R00 R24 R15 H101">58,2</td>\
			</tr><tr>\
				<th class="layout2-table-stub layout2-table-stub3" id="R26" rowspan="1" scope="row">Čezmerna hranjenost</th><td class="table-data-filled" headers="R00 R24 R26 H101">30,4</td>\
			</tr><tr>\
				<th class="layout2-table-stub layout2-table-stub3" id="R37" rowspan="1" scope="row">Debelost</th><td class="table-data-filled" headers="R00 R24 R37 H101">8,1</td>\
			</tr><tr>\
				<th class="layout2-table-stub layout2-table-stub2" id="R38" rowspan="4" scope="row">35-44 let</th><th class="layout2-table-stub layout2-table-stub3" id="R08" rowspan="1" scope="row">Podhranjenost</th><td class="table-data-filled" headers="R00 R38 R08 H101">0,9</td>\
			</tr><tr>\
				<th class="layout2-table-stub layout2-table-stub3" id="R19" rowspan="1" scope="row">Normalna hranjenost</th><td class="table-data-filled" headers="R00 R38 R19 H101">42,8</td>\
			</tr><tr>\
				<th class="layout2-table-stub layout2-table-stub3" id="R210" rowspan="1" scope="row">Čezmerna hranjenost</th><td class="table-data-filled" headers="R00 R38 R210 H101">40,1</td>\
			</tr><tr>\
				<th class="layout2-table-stub layout2-table-stub3" id="R311" rowspan="1" scope="row">Debelost</th><td class="table-data-filled" headers="R00 R38 R311 H101">16,2</td>\
			</tr><tr>\
				<th class="layout2-table-stub layout2-table-stub2" id="R412" rowspan="4" scope="row">45-54 let</th><th class="layout2-table-stub layout2-table-stub3" id="R012" rowspan="1" scope="row">Podhranjenost</th><td class="table-data-filled" headers="R00 R412 R012 H101">1,2</td>\
			</tr><tr>\
				<th class="layout2-table-stub layout2-table-stub3" id="R113" rowspan="1" scope="row">Normalna hranjenost</th><td class="table-data-filled" headers="R00 R412 R113 H101">37,1</td>\
			</tr><tr>\
				<th class="layout2-table-stub layout2-table-stub3" id="R214" rowspan="1" scope="row">Čezmerna hranjenost</th><td class="table-data-filled" headers="R00 R412 R214 H101">37,4</td>\
			</tr><tr>\
				<th class="layout2-table-stub layout2-table-stub3" id="R315" rowspan="1" scope="row">Debelost</th><td class="table-data-filled" headers="R00 R412 R315 H101">24,3</td>\
			</tr><tr>\
				<th class="layout2-table-stub layout2-table-stub2" id="R516" rowspan="4" scope="row">55-64 let</th><th class="layout2-table-stub layout2-table-stub3" id="R016" rowspan="1" scope="row">Podhranjenost</th><td class="table-data-filled" headers="R00 R516 R016 H101">0,8</td>\
			</tr><tr>\
				<th class="layout2-table-stub layout2-table-stub3" id="R117" rowspan="1" scope="row">Normalna hranjenost</th><td class="table-data-filled" headers="R00 R516 R117 H101">28,6</td>\
			</tr><tr>\
				<th class="layout2-table-stub layout2-table-stub3" id="R218" rowspan="1" scope="row">Čezmerna hranjenost</th><td class="table-data-filled" headers="R00 R516 R218 H101">41,9</td>\
			</tr><tr>\
				<th class="layout2-table-stub layout2-table-stub3" id="R319" rowspan="1" scope="row">Debelost</th><td class="table-data-filled" headers="R00 R516 R319 H101">28,8</td>\
			</tr><tr>\
				<th class="layout2-table-stub layout2-table-stub2" id="R620" rowspan="4" scope="row">65-74 let</th><th class="layout2-table-stub layout2-table-stub3" id="R020" rowspan="1" scope="row">Podhranjenost</th><td class="table-data-filled" headers="R00 R620 R020 H101">0,8</td>\
			</tr><tr>\
				<th class="layout2-table-stub layout2-table-stub3" id="R121" rowspan="1" scope="row">Normalna hranjenost</th><td class="table-data-filled" headers="R00 R620 R121 H101">30,7</td>\
			</tr><tr>\
				<th class="layout2-table-stub layout2-table-stub3" id="R222" rowspan="1" scope="row">Čezmerna hranjenost</th><td class="table-data-filled" headers="R00 R620 R222 H101">42,7</td>\
			</tr><tr>\
				<th class="layout2-table-stub layout2-table-stub3" id="R323" rowspan="1" scope="row">Debelost</th><td class="table-data-filled" headers="R00 R620 R323 H101">25,9</td>\
			</tr><tr>\
				<th class="layout2-table-stub layout2-table-stub2" id="R724" rowspan="4" scope="row">75+ let</th><th class="layout2-table-stub layout2-table-stub3" id="R024" rowspan="1" scope="row">Podhranjenost</th><td class="table-data-filled" headers="R00 R724 R024 H101">1,0</td>\
			</tr><tr>\
				<th class="layout2-table-stub layout2-table-stub3" id="R125" rowspan="1" scope="row">Normalna hranjenost</th><td class="table-data-filled" headers="R00 R724 R125 H101">34,8</td>\
			</tr><tr>\
				<th class="layout2-table-stub layout2-table-stub3" id="R226" rowspan="1" scope="row">Čezmerna hranjenost</th><td class="table-data-filled" headers="R00 R724 R226 H101">43,4</td>\
			</tr><tr>\
				<th class="layout2-table-stub layout2-table-stub3" id="R327" rowspan="1" scope="row">Debelost</th><td class="table-data-filled" headers="R00 R724 R327 H101">20,8</td>\
			</tr>\
		</tbody>');
	
		
}
/*



*/